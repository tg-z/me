# me
.
├── .git
│   ├── hooks
│   │   ├── applypatch-msg.sample
│   │   ├── commit-msg.sample
│   │   ├── fsmonitor-watchman.sample
│   │   ├── post-update.sample
│   │   ├── pre-applypatch.sample
│   │   ├── pre-commit.sample
│   │   ├── pre-push.sample
│   │   ├── pre-rebase.sample
│   │   ├── pre-receive.sample
│   │   ├── prepare-commit-msg.sample
│   │   └── update.sample
│   ├── info
│   │   └── exclude
│   ├── logs
│   │   ├── refs
│   │   │   ├── heads
│   │   │   │   └── master
│   │   │   └── remotes
│   │   │       └── origin
│   │   │           ├── HEAD
│   │   │           └── master
│   │   └── HEAD
│   ├── objects
│   │   ├── 13
│   │   │   └── 75094a7cc80681b93f761540fe6850dccace5b
│   │   ├── 14
│   │   │   └── 4f0c080f5cb452d787e9445a54cd916833b254
│   │   ├── 21
│   │   │   └── c4abd409de7e6bd8aabb9093e8085b3793caf8
│   │   ├── 2c
│   │   │   └── 2f143c987117d2acb16696a13065d2b6c912b9
│   │   ├── 2e
│   │   │   └── 524db7b334a567330fc30fa09244c4b7a85d45
│   │   ├── 4a
│   │   │   └── 40b4100b1eb04ecca88f3e3422dc927b19bb00
│   │   ├── 4f
│   │   │   └── 48ca182d20e9a8a59fb366bebeb20405c028a9
│   │   ├── 55
│   │   │   └── 9dabbbfc989daeb1dfc773b8b84e88562a0d3c
│   │   ├── 56
│   │   │   └── 646d82e7820c58f1e08099bac9980545376f09
│   │   ├── 5b
│   │   │   └── 46bfc4e0efe13aa52006d00cd6859dd77d989f
│   │   ├── 63
│   │   │   └── 91acdd5e7b0b6a306fa024d22853718864ea90
│   │   ├── 6f
│   │   │   └── da22b5fcd864049ed6fb6eedbefb4ab7b15575
│   │   ├── 84
│   │   │   └── aa92d2d3c0cb7ac2dd589410ef3b20fa82efa7
│   │   ├── 97
│   │   │   └── 48e88d3e95548fbda0d82412b5266cb86569fe
│   │   ├── ad
│   │   │   └── 68baacc7b9f29ec635851a8a77da202ff8e84a
│   │   ├── b4
│   │   │   └── a38db034dc140a0beeda9092fa6a425bdc500f
│   │   ├── bb
│   │   │   ├── 117428cbc8729594f28c221510d9d385a52bfb
│   │   │   └── 33ec4fec019dcc146429fb99a4717a2c2956e2
│   │   ├── be
│   │   │   └── 9775927a33ca3166d16040f02c0a8321f1a40e
│   │   ├── bf
│   │   │   └── 0a5ec1b593d9d06ff9535ca13f47cd0629b77c
│   │   ├── c1
│   │   │   └── beacbb083efb4aa8cd9369bdf43c761d15f15e
│   │   ├── e5
│   │   │   └── 151a016bc91a05d56ad10dfae4aa6214bca558
│   │   ├── ee
│   │   │   └── 8fbf1a93fb51bad410b04707a36fda73319f17
│   │   ├── info
│   │   └── pack
│   ├── refs
│   │   ├── heads
│   │   │   └── master
│   │   ├── remotes
│   │   │   └── origin
│   │   │       ├── HEAD
│   │   │       └── master
│   │   └── tags
│   ├── COMMIT_EDITMSG
│   ├── config
│   ├── description
│   ├── FETCH_HEAD
│   ├── HEAD
│   ├── index
│   ├── ORIG_HEAD
│   └── packed-refs
├── .history
│   ├── index_20191001020035.html
│   ├── index_20191006151217.html
│   ├── index_20191006152057.html
│   ├── index_20191006152406.html
│   ├── index_20191006152607.html
│   ├── index_20191006152631.html
│   ├── index_20191006152811.html
│   ├── index_20191006152939.html
│   ├── index_20191006153055.html
│   ├── index_20191006153236.html
│   ├── index_20191006153932.html
│   └── index_20191006154031.html
├── me-files
│   ├── randomizer
│   │   ├── randomizer.html
│   │   ├── scripts.js
│   │   └── styles.css
│   ├── .DS_Store
│   └── style.css
├── .DS_Store
├── index.html
└── README.md



📦me
 ┣ 📂.git
 ┃ ┣ 📂hooks
 ┃ ┃ ┣ 📜applypatch-msg.sample
 ┃ ┃ ┣ 📜commit-msg.sample
 ┃ ┃ ┣ 📜fsmonitor-watchman.sample
 ┃ ┃ ┣ 📜post-update.sample
 ┃ ┃ ┣ 📜pre-applypatch.sample
 ┃ ┃ ┣ 📜pre-commit.sample
 ┃ ┃ ┣ 📜pre-push.sample
 ┃ ┃ ┣ 📜pre-rebase.sample
 ┃ ┃ ┣ 📜pre-receive.sample
 ┃ ┃ ┣ 📜prepare-commit-msg.sample
 ┃ ┃ ┗ 📜update.sample
 ┃ ┣ 📂info
 ┃ ┃ ┗ 📜exclude
 ┃ ┣ 📂logs
 ┃ ┃ ┣ 📂refs
 ┃ ┃ ┃ ┣ 📂heads
 ┃ ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┃ ┗ 📂remotes
 ┃ ┃ ┃ ┃ ┗ 📂origin
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HEAD
 ┃ ┃ ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┗ 📜HEAD
 ┃ ┣ 📂objects
 ┃ ┃ ┣ 📂13
 ┃ ┃ ┃ ┗ 📜75094a7cc80681b93f761540fe6850dccace5b
 ┃ ┃ ┣ 📂14
 ┃ ┃ ┃ ┗ 📜4f0c080f5cb452d787e9445a54cd916833b254
 ┃ ┃ ┣ 📂21
 ┃ ┃ ┃ ┗ 📜c4abd409de7e6bd8aabb9093e8085b3793caf8
 ┃ ┃ ┣ 📂2c
 ┃ ┃ ┃ ┗ 📜2f143c987117d2acb16696a13065d2b6c912b9
 ┃ ┃ ┣ 📂2e
 ┃ ┃ ┃ ┗ 📜524db7b334a567330fc30fa09244c4b7a85d45
 ┃ ┃ ┣ 📂4a
 ┃ ┃ ┃ ┗ 📜40b4100b1eb04ecca88f3e3422dc927b19bb00
 ┃ ┃ ┣ 📂4f
 ┃ ┃ ┃ ┗ 📜48ca182d20e9a8a59fb366bebeb20405c028a9
 ┃ ┃ ┣ 📂55
 ┃ ┃ ┃ ┗ 📜9dabbbfc989daeb1dfc773b8b84e88562a0d3c
 ┃ ┃ ┣ 📂56
 ┃ ┃ ┃ ┗ 📜646d82e7820c58f1e08099bac9980545376f09
 ┃ ┃ ┣ 📂5b
 ┃ ┃ ┃ ┗ 📜46bfc4e0efe13aa52006d00cd6859dd77d989f
 ┃ ┃ ┣ 📂63
 ┃ ┃ ┃ ┗ 📜91acdd5e7b0b6a306fa024d22853718864ea90
 ┃ ┃ ┣ 📂6f
 ┃ ┃ ┃ ┗ 📜da22b5fcd864049ed6fb6eedbefb4ab7b15575
 ┃ ┃ ┣ 📂84
 ┃ ┃ ┃ ┗ 📜aa92d2d3c0cb7ac2dd589410ef3b20fa82efa7
 ┃ ┃ ┣ 📂97
 ┃ ┃ ┃ ┗ 📜48e88d3e95548fbda0d82412b5266cb86569fe
 ┃ ┃ ┣ 📂ad
 ┃ ┃ ┃ ┗ 📜68baacc7b9f29ec635851a8a77da202ff8e84a
 ┃ ┃ ┣ 📂b4
 ┃ ┃ ┃ ┗ 📜a38db034dc140a0beeda9092fa6a425bdc500f
 ┃ ┃ ┣ 📂bb
 ┃ ┃ ┃ ┣ 📜117428cbc8729594f28c221510d9d385a52bfb
 ┃ ┃ ┃ ┗ 📜33ec4fec019dcc146429fb99a4717a2c2956e2
 ┃ ┃ ┣ 📂be
 ┃ ┃ ┃ ┗ 📜9775927a33ca3166d16040f02c0a8321f1a40e
 ┃ ┃ ┣ 📂bf
 ┃ ┃ ┃ ┗ 📜0a5ec1b593d9d06ff9535ca13f47cd0629b77c
 ┃ ┃ ┣ 📂c1
 ┃ ┃ ┃ ┗ 📜beacbb083efb4aa8cd9369bdf43c761d15f15e
 ┃ ┃ ┣ 📂e5
 ┃ ┃ ┃ ┗ 📜151a016bc91a05d56ad10dfae4aa6214bca558
 ┃ ┃ ┣ 📂ee
 ┃ ┃ ┃ ┗ 📜8fbf1a93fb51bad410b04707a36fda73319f17
 ┃ ┃ ┣ 📂info
 ┃ ┃ ┗ 📂pack
 ┃ ┣ 📂refs
 ┃ ┃ ┣ 📂heads
 ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┣ 📂remotes
 ┃ ┃ ┃ ┗ 📂origin
 ┃ ┃ ┃ ┃ ┣ 📜HEAD
 ┃ ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┗ 📂tags
 ┃ ┣ 📜COMMIT_EDITMSG
 ┃ ┣ 📜FETCH_HEAD
 ┃ ┣ 📜HEAD
 ┃ ┣ 📜ORIG_HEAD
 ┃ ┣ 📜config
 ┃ ┣ 📜description
 ┃ ┣ 📜index
 ┃ ┗ 📜packed-refs
 ┣ 📂.history
 ┃ ┣ 📜README_20190930044334.md
 ┃ ┣ 📜README_20191006155251.md
 ┃ ┣ 📜README_20191006155255.md
 ┃ ┣ 📜index_20191001020035.html
 ┃ ┣ 📜index_20191006151217.html
 ┃ ┣ 📜index_20191006152057.html
 ┃ ┣ 📜index_20191006152406.html
 ┃ ┣ 📜index_20191006152607.html
 ┃ ┣ 📜index_20191006152631.html
 ┃ ┣ 📜index_20191006152811.html
 ┃ ┣ 📜index_20191006152939.html
 ┃ ┣ 📜index_20191006153055.html
 ┃ ┣ 📜index_20191006153236.html
 ┃ ┣ 📜index_20191006153932.html
 ┃ ┗ 📜index_20191006154031.html
 ┣ 📂me-files
 ┃ ┣ 📂randomizer
 ┃ ┃ ┣ 📜randomizer.html
 ┃ ┃ ┣ 📜scripts.js
 ┃ ┃ ┗ 📜styles.css
 ┃ ┣ 📜.DS_Store
 ┃ ┗ 📜style.css
 ┣ 📜.DS_Store
 ┣ 📜README.md
 ┗ 📜index.html